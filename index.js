import { application } from "./hcl/Application.js";
import { TForm } from "./hcl/Forms.js";
import { hcl } from "./hcl/HCL.js";
import { TToolBar, TLable, TUrlLable } from "./hcl/StdCtrls.js";
import { TAlign } from "./hcl/Controls.js";
import { THttp } from "./hcl/ExtCtrls.js";

export class TFrmInchStation extends TForm {
    constructor(width, height) {
        super(width, height);
        this.captionBar.captureParent = false;
        this.captionBar.controls.clear();
        this.captionBar.addButton("", false, application.icon.src).onClick = function() { hcl.showMessage("欢迎使用HCView！"); }

        let lblHint = new TLable("以下是HCView控件相关的演示demo链接，更多技术交流请加入QQ群 649023932");
        lblHint.font.size = 12;
        lblHint.left = 20;
        lblHint.top = 20;
        lblHint.autoSize = true;
        this.addControl(lblHint);

        let urlHCView = new TUrlLable("普通文本排版（页眉页脚）");
        urlHCView.hint = "具有页眉页脚的文本排版演示";
        urlHCView.url = "./hcview/frmHCView.html";
        urlHCView.left = 40;
        urlHCView.top = lblHint.bottom + 20;;
        this.addControl(urlHCView);

        let urlHCEdit = new TUrlLable("普通文本排版（无页眉页脚）");
        urlHCEdit.hint = "富文本排版编辑演示";
        urlHCEdit.url = "./hcview/frmHCEdit.html";
        urlHCEdit.left = 40;
        urlHCEdit.top = urlHCView.bottom + 10;;
        this.addControl(urlHCEdit);

        let urlHCEMRView = new TUrlLable("结构化病历编辑器");
        urlHCEMRView.hint = "HCView用于电子病历编辑器的功能演示";
        urlHCEMRView.url = "./hcemrview/frmInchStation.html";
        urlHCEMRView.left = 40;
        urlHCEMRView.top = urlHCEdit.bottom + 10;;
        this.addControl(urlHCEMRView);

        let lblMore = new TLable("其他demo链接，更多技术交流请加入QQ群 649023932");
        lblMore.font.size = 12;
        lblMore.left = 20;
        lblMore.top = urlHCEMRView.bottom + 20;
        lblMore.autoSize = true;
        this.addControl(lblMore);

        let urlPlayer = new TUrlLable("视觉播放器");
        urlPlayer.hint = "基于HCL前端框架制作的mp3播放器演示";
        urlPlayer.url = "./demo/MusicPlayer/musicPlayer.html";
        urlPlayer.left = 40;
        urlPlayer.top = lblMore.bottom + 10;;
        this.addControl(urlPlayer);

        let urlHCL = new TUrlLable("HCL前端框架");
        urlHCL.hint = "HCL前端框架各种控件演示";
        urlHCL.url = "./hcl/_hclDemo.html";
        urlHCL.left = 40;
        urlHCL.top = urlPlayer.bottom + 10;;
        this.addControl(urlHCL);

        let statebar = new TToolBar();
        statebar.align = TAlign.Bottom;
        let lable = new TLable("Copyright© 2019-2020 HCView ");
        lable.font.name = "Arial";
        lable.align = TAlign.Left;
        statebar.addControl(lable);
        let urllable = new TUrlLable("京ICP备19050288号");
        urllable.hint = "京ICP备19050288号";
        urllable.url = "http://beian.miit.gov.cn";
        urllable.font.name = "Arial";
        urllable.align = TAlign.Left;
        statebar.addControl(urllable);
        this.addControl(statebar);
    }
}

THttp.get("http://123.206.99.69:12860/", { CMD: "9"}, (responseText) => { });

application.icon.src = "./image/hcview.png";
let mainForm = new TFrmInchStation(hcl.width, hcl.height);
application.addForm(mainForm);
hcl.homePath = "./hcl/";
hcl.autoWidth = true;
application.run();